#define COLOR(col) "^c" col "^"

#define volumeblock    "source $(which bar-volume) && echo " COLOR("#3F4B4B") " $icon ^d^$volume"
#define networkblock   "source $(which bar-network) && echo " COLOR("#3F4B4B") " $icon ^d^$network"
#define memblock       "echo -e " COLOR("#3F4B4B") "  ^d^$(free -m | awk '/^Mem/ { used=($2-$7)/1000; total=$2/1000; printf \"%0.2fG/%0.2fG\", used, total }')"
#define timeblock      "echo -e " COLOR("#3F4B4B") " $(date +' ^d^%R')"
#define dateblock      "echo -e " COLOR("#3F4B4B") " $(date +'^d^ %A, %d-%m-%Y')"
static const Block blocks[] = {
    /*Icon*/   /*Command*/  /*Update Interval*/   /*Update Signal*/
	{"",      volumeblock,    0,                  15},
	{"",      networkblock,   3,                   0},
	{"",      memblock,       10,                  0},
	{"",      timeblock,      10,                  0},
	{"",      dateblock,      21600,                0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = "  ";
static unsigned int delimLen = 5;
